﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public string bulletName = "GP90";
    public float muzzleVelocity = 905; // m/s

    public Projectile projectilePrefab;
    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            FireBullet(transform.position, transform.rotation);
        }
    }

    public void FireBullet(Vector3 origin, Quaternion quaternion)
    {
        Projectile projectile = Instantiate(projectilePrefab, origin, quaternion);
        projectile.velocity = projectile.transform.forward * muzzleVelocity;
    }
}
