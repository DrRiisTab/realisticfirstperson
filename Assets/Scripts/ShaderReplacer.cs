using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShaderReplacer : MonoBehaviour
{
    public Shader Shader;

    // Start is called before the first frame update
    void Start()
    {
        Camera.main.SetReplacementShader(Shader, "RenderType");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
