﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public TrailRenderer trailRenderer;

    public float projectileMass = 0.0041f; // kilograms
    public float airFrictionCoefficency = 0.3f;
    public float projectileDiameter = 0.00556f; // meter

    public float maxLifeTime = 10f; //s

    public float kConstant => Environment.instance.airDensity * airFrictionCoefficency * projectileArea * 0.5f; //kg/m
    public float projectileArea => Mathf.Pow(projectileDiameter * 0.5f, 2) * Mathf.PI; // m^2
    public float kineticEnergy => projectileMass * velocitySquared * 0.5f; // joule
    public float velocitySquared => Mathf.Pow(velocity.magnitude, 2); // m^2/s^2
    public float airResistance => kConstant * velocitySquared; // N
    public float airDeceleration => airResistance / projectileMass; // m/s^2

    public float GetKConstant(float v)
    {
        return Environment.instance.airDensity* airFrictionCoeffientAnim.Evaluate(v * 1000f) *projectileArea * 0.5f; //kg/m
    }

    public float GetAirDeceleration(float v) => GetKConstant(v) * v * v / projectileMass;

    public Vector3 GetDeceleration(Vector3 v) => Vector3.down * Environment.instance.gravitationAcceleration - v.normalized * GetAirDeceleration(v.magnitude);

    public AudioClip[] impactSounds;

    public Vector3 velocity;

    public float currentLifeTime = 0;

    List<TrailPosition> trailPositions = new List<TrailPosition>();

    public GameObject onHitPrefab;

    public AnimationCurve airFrictionCoeffientAnim;

    private void Start()
    {
        if(trailRenderer != null)
        {
            trailRenderer.enabled = true;
        }
    }

    private void FixedUpdate()
    {
        //Debug.Log($"TOF: {currentLifeTime}; Distance: {transform.position.z}; Drop: {transform.position.y}; Velocity: {velocity.magnitude}; Energy: {kineticEnergy}");

        if(currentLifeTime >= maxLifeTime)
        {
            Destroy(gameObject);
        }

        float currentVelocity = velocity.magnitude;

        RaycastHit raycastHit;
        if(Physics.Raycast(transform.position, transform.forward, out raycastHit, currentVelocity * Time.fixedDeltaTime)){

            transform.position = raycastHit.point;

            GameObject victim = raycastHit.collider.gameObject;
            Hitable hitable = victim.GetComponent<Hitable>();
            if(hitable != null)
            {
                hitable.OnHit(this, raycastHit);
            }
            else
            {
                GameObject hitParticles = Instantiate(onHitPrefab, transform.position, Quaternion.LookRotation(raycastHit.normal));
                AudioSource audioSource = hitParticles.GetComponent<AudioSource>();
                audioSource.PlayOneShot(impactSounds[Random.Range(0, impactSounds.Length)]);
            }
            Destroy(gameObject);
        }
        else
        {
            trailPositions.Add(new TrailPosition(transform.position, kineticEnergy, currentLifeTime)
            {
                Velocity = velocity.magnitude
            });

            Vector3 decelK = GetDeceleration(velocity);
            Vector3 decel05 = GetDeceleration(velocity + Time.fixedDeltaTime * 0.5f * decelK);
            velocity += Time.fixedDeltaTime * decel05;

            transform.position += velocity * Time.fixedDeltaTime;
        }

        currentLifeTime += Time.fixedDeltaTime;
    }

    private void Update()
    {
        
    }

    private void OnDrawGizmos()
    {
        if (!trailPositions.Any())
        {
            return;
        }

        float maxKE = trailPositions.Max(s => s.KineticEnergy);
        float minKE = trailPositions.Min(s => s.KineticEnergy);
        float lastTof = 0;
        float tofInterval = 0.1f;

        for (int i = 0; i < trailPositions.Count - 1; i++)
        {
            Gizmos.color = Color.Lerp(Color.blue, Color.red, Mathf.InverseLerp(minKE, maxKE, trailPositions[i].KineticEnergy));
            Gizmos.DrawLine(trailPositions[i].Position, trailPositions[i + 1].Position);
            if(trailPositions[i].TimeOfFlight > lastTof)
            {
                lastTof += tofInterval;
                Gizmos.DrawSphere(trailPositions[i].Position, 1f);
                //Handles.Label(trailPositions[i].Position, trailPositions[i].TimeOfFlight.ToString("0.0s"));
                //Handles.Label(trailPositions[i].Position, trailPositions[i].Position.y.ToString("0.00m"));
            }
        }
    }
}

public class TrailPosition
{
    public Vector3 Position { get; set; }
    public float KineticEnergy { get; set; }
    public float TimeOfFlight { get; set; }

    public float Velocity { get; set; }

    public TrailPosition(Vector3 position, float kineticEnergy, float tof)
    {
        Position = position;
        KineticEnergy = kineticEnergy;
        TimeOfFlight = tof;
    }
}
