using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunSwitcher : MonoBehaviour
{
    public Gun[] Guns;
    private int gunIndex;

    public int GunIndex
    {
        get { return gunIndex; }
        set { gunIndex = (Guns.Length + value) % Guns.Length; }
    }

    GunController _gunController;

    // Start is called before the first frame update
    void Start()
    {
        _gunController = GetComponent<GunController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Switch Gun"))
        {
            GunIndex++;
            _gunController.gun = Guns[GunIndex];
        }
    }
}
