﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Audio;

[RequireComponent(typeof(AudioSource))]
public class Tinnitus : MonoBehaviour
{
    AudioSource audioSource;

    public AudioMixerGroup audioMixerGroupGun;

    public AnimationCurve impairmentRatio;
    public AnimationCurve hearingLossRatio;

    public float impairmentFactor => impairments.Select(s => s.GetImpairment(Time.realtimeSinceStartup)).Sum();

    public float impairmentDropThreshold = 0.001f;

    public List<Impairment> impairments = new List<Impairment>();

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        impairments.RemoveAll(s => s.GetImpairment(Time.realtimeSinceStartup) < impairmentDropThreshold);
        audioSource.volume = impairmentRatio.Evaluate(impairmentFactor);

        audioMixerGroupGun.audioMixer.SetFloat("GunMasterVolume", hearingLossRatio.Evaluate(impairmentFactor));
    }

    public void AddImpairment(float volume)
    {

        impairments.Add(new Impairment()
        {
            Volume = Mathf.Pow(volume, 0.3f),
            Time = Time.realtimeSinceStartup
        });
    }
}

public struct Impairment
{
    public float Volume { get; set; }
    public float Time { get; set; }

    public float GetImpairment(float currentTime)
    {
        return Volume / (Mathf.Pow(currentTime - Time, 0.5f) + 1);
    }
}