using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ElevationDisplayer : MonoBehaviour
{
    public Transform ElevationTransform;
    public Transform WindageTransform;

    public Text ElevationText;
    public Text WindageText;

    public Scope Scope;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        ElevationText.transform.position = Camera.main.WorldToScreenPoint(ElevationTransform.position);
        WindageText.transform.position = Camera.main.WorldToScreenPoint(WindageTransform.position);
        ElevationText.text = (Scope.Zeroing.y * 1000f).ToString("0.0");
        WindageText.text = (Scope.Zeroing.x * 1000f).ToString("0.0");
    }
}
