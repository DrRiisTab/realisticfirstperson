using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadShake : MonoBehaviour
{
    public Transform EyeReliefTransform;
    public float maxDistance = 0.1f;
    public float lerpSpeed = 0.1f;

    float nextUpdate;
    Vector3 targetPosition;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Time.time > nextUpdate)
        {
            nextUpdate = Time.time + 0.5f;
            targetPosition = EyeReliefTransform.position + new Vector3(Random.value - 0.5f, Random.value - 0.5f, 0) * maxDistance;
        }

        transform.position = Vector3.Lerp(transform.position, targetPosition, lerpSpeed * Time.deltaTime);
    }
}
