﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Environment : MonoBehaviour
{
    public static Environment instance;

    public float airDensity = 1.013f; //hPa
    public float gravitationAcceleration = 9.81f; //m/s^2

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            if(instance != this)
            {
                Destroy(this);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
