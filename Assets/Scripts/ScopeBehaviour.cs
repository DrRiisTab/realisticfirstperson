﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScopeBehaviour : MonoBehaviour
{
    public Camera scopeCamera;
    public Camera mainCamera;
    public Transform crossHairTransform;
    public Transform cameraGoalTransform;
    public Transform imageTransform;

    public GameObject lens;

    public float eyeDistance = 0.03f;
    public float coverFactor = 10f;
    public float parallaxDistance = 50f;
    private Material lensMaterial;

    // Start is called before the first frame update
    void Start()
    {
        lensMaterial = lens.GetComponent<MeshRenderer>().material;
    }

    // Update is called once per frame
    void Update()
    {
        //Vector3 focus = cameraGoalTransform.position + cameraGoalTransform.forward * parallaxDistance;
        //Vector3 focusToMain = mainCamera.transform.position - focus;
        //scopeCamera.transform.position = focus + focusToMain.normalized * parallaxDistance;
        //scopeCamera.transform.LookAt(focus, cameraGoalTransform.up);

        float distance = Vector3.Distance(lens.transform.position, mainCamera.transform.position) * coverFactor;
        lensMaterial.SetFloat("_CircleRadius", distance);

        //Vector3 optimalViewDirection = imageTransform.position - lens.transform.position;
        //Vector3 actualDirection = lens.transform.position - mainCamera.transform.position;



        //lensMaterial.SetTextureOffset("_MainTex", );
    }

    public void ScopeIn()
    {
        mainCamera.enabled = false;
        scopeCamera.enabled = true;
    }

    public void ScopeOut()
    {
        scopeCamera.enabled = false;
        mainCamera.enabled = true;
    }

    void FixedUpdate()
    {
    }
}
