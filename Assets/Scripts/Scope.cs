using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[ExecuteInEditMode]
public class Scope : MonoBehaviour
{
    public Lens Lens;
    public float LensRadius;
    public Camera ScopeCamera;
    public Transform ImagePlane;
    public Transform EyeRelief;

    public Vector2 Zeroing = Vector2.zero;

    public float Scale;
    public float ParallaxDistance;
    [Range(1,25)]
    public float Magnification = 3f;

    public Reticle[] Reticles;

    public AudioClip AudioClipSingleClick;
    public AudioClip AudioClipTenClicks;

    public float FovMRad;
    public float MouseFov { get; private set; }

    AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();

        ScopeCamera.depthTextureMode = DepthTextureMode.Depth;

        RenderTexture renderTexture = new RenderTexture(2048, 2048, 16);
        //Debug.Log(width);
        ScopeCamera.targetTexture = renderTexture;
        Lens.MeshRenderer.sharedMaterial.mainTexture = renderTexture;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (Input.GetButton("Magnification up"))
        {
            Magnification+=Time.deltaTime*10;
        }

        if (Input.GetButton("Magnification down"))
        {
            Magnification-=Time.deltaTime*10;
        }

        Magnification = Mathf.Clamp(Magnification, 5, 25);

        AudioClip clickClip = AudioClipSingleClick;
        float zeroingStep = 0.0001f;
        if (Input.GetButton("Sprint"))
        {
            zeroingStep =  0.001f;
            clickClip = AudioClipTenClicks;
        }

        if (Input.GetButtonDown("Zero up"))
        {
            Zeroing.y += zeroingStep;
            audioSource.PlayOneShot(clickClip);
        }

        if(Input.GetButtonDown("Zero down"))
        {
            Zeroing.y -= zeroingStep;
            audioSource.PlayOneShot(clickClip);
        }

        float lensAngle = 2 * Vector3.Angle(Lens.transform.position - Camera.main.transform.position, Lens.transform.position + Camera.main.transform.right.normalized * LensRadius - Camera.main.transform.position);

        ScopeCamera.fieldOfView = lensAngle / Magnification;
        FovMRad = 17.4533f * ScopeCamera.fieldOfView;
        MouseFov = ScopeCamera.fieldOfView * Camera.main.fieldOfView / lensAngle;

        Vector3 targetDirection = ImagePlane.position - Camera.main.transform.position;
        Vector3 target = Lens.transform.position + (Lens.transform.forward - Lens.transform.up * Zeroing.y + Lens.transform.right * Zeroing.x) * ParallaxDistance;
        ScopeCamera.transform.position = ImagePlane.position + targetDirection;
        ScopeCamera.transform.LookAt(target, Vector3.up);
        //ScopeCamera.transform.Rotate(ScopeCamera.transform.right, Zeroing.y * 17.4533f);
        //ScopeCamera.transform.Rotate(ScopeCamera.transform.up, Zeroing.x * 17.4533f);

        float lensWidthPixels = lensAngle * Camera.main.pixelWidth / Camera.main.fieldOfView;
        float lensHeightPixels = lensWidthPixels;

        Vector3 screenImagePlaneCenter = Camera.main.WorldToScreenPoint(ImagePlane.position);
        Vector3 screenLensCenter = Camera.main.WorldToScreenPoint(Lens.transform.position);
        Vector3 screenOffset = screenImagePlaneCenter - screenLensCenter;
        Vector2 imageOffset = new Vector2(screenOffset.x / lensWidthPixels, screenOffset.y / lensHeightPixels);
        Lens.MeshRenderer.sharedMaterial.SetVector("_ReticleOffset", imageOffset);

        Vector3 screenTargetCenter = Camera.main.WorldToScreenPoint(transform.forward * ParallaxDistance);
        Vector3 screenTargetOffset = screenTargetCenter - screenLensCenter;
        Vector2 reticleOffset = new Vector2(screenTargetOffset.x / lensWidthPixels, screenTargetOffset.y / lensHeightPixels);
        //Lens.MeshRenderer.sharedMaterial.SetVector("_ImageOffset", (Zeroing - reticleOffset)/ (17.4533f * ScopeCamera.fieldOfView));

        Lens.MeshRenderer.sharedMaterial.SetFloat("_ShadeRadius", 0.5f / (Vector3.Distance(Camera.main.transform.position, EyeRelief.position) * 2 * Magnification + 1));

        Reticle reticle = Reticles.FirstOrDefault(s => ScopeCamera.fieldOfView > s.ReticleDegrees);
        if(reticle == null)
        {
            reticle = Reticles.Last();
        }
        Lens.MeshRenderer.sharedMaterial.SetFloat("_ReticleSize", ScopeCamera.fieldOfView / reticle.ReticleDegrees);
        Lens.MeshRenderer.sharedMaterial.SetTexture("_Reticle", reticle.Texture);
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(Lens.transform.position, LensRadius);
    }
}

[Serializable]
public class Reticle
{
    public Texture Texture;
    public float MilliRadians;
    public float ReticleDegrees => MilliRadians * 180f / (1000 * Mathf.PI);
}