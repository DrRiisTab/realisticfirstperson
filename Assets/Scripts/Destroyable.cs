﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyable : Hitable
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void OnHit(Projectile projectile, RaycastHit raycastHit)
    {
        base.OnHit(projectile, raycastHit);

        Destroy(this.gameObject);
    }
}
