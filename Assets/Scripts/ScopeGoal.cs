﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScopeGoal : MonoBehaviour
{
    public ScopeBehaviour scopeBehaviour;
    public Camera playerCamera;

    public RenderTexture renderTexture;

    public float adaptSpeed = 1.5f;
    public float lerpSpeed = 10f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.LeftControl))
        {
            transform.position += transform.forward * Input.GetAxis("Horizontal") * Time.deltaTime;
            transform.position += transform.up * Input.GetAxis("Vertical") * Time.deltaTime;
        }

        Vector3 scopeToCamera = playerCamera.transform.position - transform.position;
        transform.position += scopeToCamera * Input.mouseScrollDelta.y * 0.1f;

        Vector3 movement = transform.position - scopeBehaviour.transform.position;
        float realDistance = Mathf.Pow(adaptSpeed + 1, 2) * Time.deltaTime;
        if(movement.sqrMagnitude > realDistance * realDistance)
        {
            scopeBehaviour.transform.position += movement.normalized * realDistance;
        }
        else
        {
            scopeBehaviour.transform.position = transform.position;
        }

        scopeBehaviour.transform.rotation = Quaternion.Lerp(scopeBehaviour.transform.rotation, transform.rotation, Time.fixedDeltaTime * lerpSpeed);
    }
}
