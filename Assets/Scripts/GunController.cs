﻿using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class GunController : MonoBehaviour
{
    public Gun gun;
    public Transform handTransform;
    public Transform handNeutralTransform;
    public Transform handTargetTransform;
    public Transform aimTransform;
    public Camera playerCamera;
    public Tinnitus tinnitus;
    public FirstPersonCharacterBehaviour firstPersonCharacterBehaviour;

    public RadialBlur radialBlur;

    public float defaultFov = 90f;
    public float inSightFov = 40f;
    public float fovFactor = 1f;

    public float targetDistance = 100f;
    public float lerpFactorPosition = 0.99f;
    public float lerpFactorRotation = 0.5f;
    public float inscopeLerpFactorRotation = 0.99f;

    public float radialBlurLerpFactor = 0.3f;
    public float radialBlurShot = 1f;

    public float visorChangeLerpFactor = 0.2f;
    public float adsTime = 0.5f;

    public float maxGunDistance = 0.1f;
    public float gunPosCorrectionSpeed = 10f;

    public float offsetLerpFactor = 10f;
    public float offsetCorrectionDelay = 0.5f;

    public AnimationCurve aimOffsetCurve;
    public AnimationCurve lookOffsetCurve;
    public float lookOffsetTime = .1f;

    Vector3 aimPoint => aimTransform.position + aimTransform.forward * targetDistance;

    Vector2 aimOffset = Vector2.zero;
    float lastShot;
    Coroutine adsCoroutine;

    Matrix4x4 previousCameraMatrix;

    // Start is called before the first frame update
    void Start()
    {
        firstPersonCharacterBehaviour = GetComponent<FirstPersonCharacterBehaviour>();
        tinnitus = GetComponent<Tinnitus>();
        playerCamera = aimTransform.GetComponent<Camera>();
        previousCameraMatrix = aimTransform.worldToLocalMatrix;
        handTransform.parent = null;

        gun.transform.position = handTransform.position;
        //handTargetTransform.parent = null;
    }

    // Update is called once per frame
    void Update()
    {
        if (gun == null) return;

        //radialBlur.amount = Mathf.Lerp(radialBlur.amount, 0, radialBlurLerpFactor);

        if (Input.GetButton("Fire1"))
        {
            if (gun.TryFire())
            {
                StartCoroutine(firstPersonCharacterBehaviour.RotateKick(new Vector2(lookOffsetCurve.Evaluate(Random.value), lookOffsetCurve.Evaluate(Random.value)), lookOffsetTime));

                //radialBlur.amount = radialBlurShot;
                //tinnitus.AddImpairment(165);
                aimOffset += new Vector2(aimOffsetCurve.Evaluate(Random.value), aimOffsetCurve.Evaluate(Random.value) * 0.3f);
                lastShot = Time.time;
            }
        }
        else
        {
            gun?.ReleaseTrigger();
        }

        if (lastShot + offsetCorrectionDelay <= Time.time)
        {
            aimOffset = Vector2.Lerp(aimOffset, Vector2.zero, offsetLerpFactor * Time.fixedDeltaTime);
        }

        fovFactor = Mathf.Clamp(fovFactor - Input.mouseScrollDelta.y * 0.1f, 0.5f, 2f);

        if (Input.GetButtonDown("Fire2") || Input.GetButtonUp("Fire2"))
        {
            fovFactor = 1f;
        }
        Quaternion aimDirection = Quaternion.LookRotation(aimTransform.forward + aimTransform.up * aimOffset.x + aimTransform.right * aimOffset.y, aimTransform.up);
        float targetFov = defaultFov;
        if (Input.GetButton("Fire2"))
        {
            if (Input.GetButtonDown("Fire2"))
            {
                if (adsCoroutine != null) StopCoroutine(adsCoroutine);

                Vector3 originalGunPosition = gun.transform.position;
                Quaternion originalGunRotation = gun.transform.rotation;
                gun.transform.rotation = aimTransform.rotation;
                gun.transform.position += aimTransform.position - gun.visorTransform.position;
                Vector3 targetWorldPosition = gun.transform.position;
                gun.transform.position = originalGunPosition;
                gun.transform.rotation = originalGunRotation;
                StartCoroutine(Ads(aimTransform.worldToLocalMatrix.MultiplyPoint(targetWorldPosition)));
            }
            targetFov = inSightFov;
            if (gun.scope == null)
            {
                firstPersonCharacterBehaviour.CurrentMouseFov = playerCamera.fieldOfView;
            }
            else
            {
                firstPersonCharacterBehaviour.CurrentMouseFov = gun.scope.MouseFov;
            }
        }
        else
        {
            if (Input.GetButtonUp("Fire2"))
            {
                if(adsCoroutine != null) StopCoroutine(adsCoroutine);
                Vector3 targetWorldPosition = handNeutralTransform.position;
                StartCoroutine(Ads(aimTransform.worldToLocalMatrix.MultiplyPoint(targetWorldPosition)));
            }
            gun?.ScopeOut();
            firstPersonCharacterBehaviour.CurrentMouseFov = playerCamera.fieldOfView;
        }
        Vector3 handDelta = handTargetTransform.position - handTransform.position;
        //handTransform.position += Vector3.ClampMagnitude(handDelta, handMovement);
        handTransform.position = Vector3.Lerp(handTransform.position + firstPersonCharacterBehaviour._rigidbody.velocity * Time.deltaTime * 0.5f, handTargetTransform.position, visorChangeLerpFactor * Time.deltaTime * Mathf.Sqrt(handDelta.magnitude));
        //handTransform.position = handTargetTransform.position - Vector3.ClampMagnitude(firstPersonCharacterBehaviour._rigidbody.velocity * Time.deltaTime, maxGunDistance);
        targetFov *= fovFactor;
        playerCamera.fieldOfView = Mathf.Lerp(playerCamera.fieldOfView, targetFov, visorChangeLerpFactor * Time.deltaTime);
        Vector3 gunHandDelta = handTransform.position - gun.transform.position;

        Vector3 previousLocalPosition = previousCameraMatrix.MultiplyPoint(gun.transform.position);
        gun.transform.position = Vector3.Lerp(aimTransform.localToWorldMatrix.MultiplyPoint(previousLocalPosition), handTransform.position, 0.1f);
        //gun.transform.position = Vector3.Lerp(gun.transform.position + firstPersonCharacterBehaviour._rigidbody.velocity * Time.deltaTime * 0.99f, handTransform.position, Mathf.Sqrt(gunHandDelta.magnitude));
        gun.transform.rotation = Quaternion.Lerp(gun.transform.rotation, aimDirection, lerpFactorRotation * Time.deltaTime);

        if (Input.GetButtonDown("ToggleFiremode"))
        {
            gun.fullAuto = !gun.fullAuto;
        }

        previousCameraMatrix = aimTransform.worldToLocalMatrix;
    }

    IEnumerator Ads(Vector3 localHandTargetPosition)
    {
        float adsDelta = 0;
        Vector3 startLocalPosition = aimTransform.worldToLocalMatrix.MultiplyPoint(handTargetTransform.position);
        while(adsDelta < adsTime)
        {
            Vector3 nextLocalPosition = Vector3.Lerp(startLocalPosition, localHandTargetPosition, adsDelta / adsTime);
            handTargetTransform.position = aimTransform.localToWorldMatrix.MultiplyPoint(nextLocalPosition);
            adsDelta += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        handTargetTransform.position = aimTransform.localToWorldMatrix.MultiplyPoint(localHandTargetPosition);
    }

    private void TryPickupGun()
    {
        
        RaycastHit raycastHit;
        if(Physics.Raycast(playerCamera.transform.position, playerCamera.transform.forward, out raycastHit, 5, LayerMask.NameToLayer("Gun")))
        {
            Gun gun = raycastHit.collider.GetComponent<Gun>();
            Debug.Log(gun);
            if (gun == null) return;
            if (gun.gunControllerOwner != null) return;
            gun.PickupGun(this);
        }
    }

    private void FixedUpdate()
    {
        
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawLine(playerCamera.transform.position, playerCamera.transform.position + playerCamera.transform.forward * 5);
    }
}
