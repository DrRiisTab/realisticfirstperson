﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hitable : MonoBehaviour
{

    public GameObject hitParticles;

    virtual public void OnHit(Projectile projectile, RaycastHit raycastHit)
    {
        Instantiate(hitParticles, projectile.transform.position, Quaternion.LookRotation(raycastHit.normal));
    }

}
