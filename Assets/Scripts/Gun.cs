﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Gun : MonoBehaviour
{
    public Transform muzzleTransform;
    public Transform gunTargetTransform;
    public Transform gunModelTransform;
    public Transform visorTransform;
    public ParticleSystem muzzleFlash;

    public ScopeBehaviour scopeBehaviour;
    public Scope scope;

    public Bullet bulletPrefab;

    public GunController gunControllerOwner = null;
    
    public Collider pickupCollider;

    public int currentCapacity = 30;
    public int maxCapacity = 30;

    public float fireRate = 60;

    public float fireInterval => 60 / fireRate;

    AudioSource audioSource;
    public AudioClip[] fireSounds;
    public AudioClip tailSound;

    float lastShot;

    public float normalizationFactor = 0.5f;
    public float blowBackLerpFactor = 0.9f;

    public float minBlowBack = 0.01f;
    public float maxBlowBack = 0.02f;

    public float minBlowBackRotation = 0.5f;
    public float maxBlowBackRotation = 0.7f;

    public bool fullAuto;

    int currentBurst = 0;

    Camera originalCamera;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        gunModelTransform.localPosition = Vector3.Lerp(gunModelTransform.localPosition, gunTargetTransform.localPosition, blowBackLerpFactor * Time.deltaTime);
        gunModelTransform.localRotation = Quaternion.Lerp(gunModelTransform.localRotation, gunTargetTransform.localRotation, blowBackLerpFactor * Time.deltaTime);
        gunTargetTransform.localPosition = Vector3.Lerp(gunTargetTransform.localPosition, Vector3.zero, normalizationFactor * Time.deltaTime);
        gunTargetTransform.localRotation= Quaternion.Lerp(gunTargetTransform.localRotation, Quaternion.identity, normalizationFactor * Time.deltaTime);
    }

    public void ScopeIn()
    {
        scopeBehaviour?.ScopeIn();
    }

    public void ScopeOut()
    {
        scopeBehaviour?.ScopeOut();
    }

    public bool TryFire()
    {
        if (Time.realtimeSinceStartup < lastShot + fireInterval)
        {
            return false;
        }

        if (currentCapacity < 1)
        {
            return false;
        }

        if (!fullAuto)
        {
            if(currentBurst > 0)
            {
                return false;
            }
        }

        lastShot = Time.realtimeSinceStartup;

        currentCapacity--;
        currentBurst++;

        muzzleFlash.Play();

        bulletPrefab.FireBullet(muzzleTransform.position, muzzleTransform.rotation);

	    AudioClip fireSound = fireSounds[Random.Range(0,fireSounds.Length)];

        //audioSource.pitch = Random.Range(0.9f, 1.1f);
        audioSource.PlayOneShot(fireSound);
        if(tailSound != null)
        {
            audioSource.PlayOneShot(tailSound);
        }

        gunTargetTransform.position = gunTargetTransform.position + gunTargetTransform.forward * -Random.Range(minBlowBack, maxBlowBack);
        gunTargetTransform.Rotate(Vector3.right, Random.Range(-maxBlowBackRotation, maxBlowBackRotation));
        gunTargetTransform.Rotate(Vector3.up, Random.Range(-maxBlowBackRotation, maxBlowBackRotation));

        return true;
    }

    public void PickupGun(GunController newOwner)
    {
        gunControllerOwner = newOwner;
        gunControllerOwner.gun = this;
        pickupCollider.enabled = false;
    }

    public void ReleaseGun()
    {
        if(gunControllerOwner != null)
        {
            gunControllerOwner.gun = null;
        }
        gunControllerOwner = null;
        pickupCollider.enabled = true;
    }

    public void ReleaseTrigger()
    {
        currentBurst = 0;
    }
}
