using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[ExecuteInEditMode]
public class HoloBehavior : MonoBehaviour
{
    public Renderer lensRenderer;

    public float reticleAngle = Mathf.PI / 180f;

    public Vector2 circleCenter;
    public Vector2 zeroOffset;
    public float radius;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        Vector3 lensToCam = Camera.main.transform.position - transform.position;
        Vector3 ray = Vector3.ProjectOnPlane(lensToCam, -transform.forward);
        Vector3 target = transform.position + ray;
        Vector2 offset = transform.worldToLocalMatrix.MultiplyPoint(target);
        circleCenter = offset + Vector2.one * 0.5f + zeroOffset;

        float actualWidth = transform.lossyScale.x;
        radius = lensToCam.magnitude * Mathf.Sin(reticleAngle) / actualWidth;

        lensRenderer.material.SetVector("_CircleCenter", circleCenter);
        lensRenderer.material.SetFloat("_CircleRadius", radius);
    }

    private void OnDrawGizmos()
    {
    }
}
