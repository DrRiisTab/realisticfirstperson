using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class FirstPersonCharacterBehaviour : MonoBehaviour
{
    public Camera Camera;
    public float MovementSpeed;
    public float MouseSensitivity;
    public float SprintModifier;
    public float ReferenceFov = 90f;
    public float CurrentMouseFov;
    
    public Rigidbody _rigidbody { get; private set; }
    Vector3 cameraOriginalLocalPosition;

    Vector3 movementVelocity;

    float lastFixedUpdate;

    // Start is called before the first frame update
    void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
        cameraOriginalLocalPosition = Camera.transform.localPosition;
        Camera.transform.parent = null;

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 inputMove = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        float movementSpeed = Mathf.Max(Mathf.Abs(inputMove.x), Mathf.Abs(inputMove.y)) * MovementSpeed;
        if (Input.GetButton("Sprint"))
        {
            movementSpeed *= SprintModifier;
        }
        Vector3 movementDirection = transform.right * inputMove.x + transform.forward * inputMove.y;
        movementVelocity = movementDirection.normalized * movementSpeed;

        Vector2 inputLook = new Vector2(Input.GetAxis("Mouse X"), -Input.GetAxis("Mouse Y")) * MouseSensitivity * (CurrentMouseFov / ReferenceFov);
        //Camera.transform.Rotate(inputLook.y, 0, 0, Space.Self);
        float deltaFixedTime = Time.time - lastFixedUpdate;
        Vector3 cameraTargetPosition = transform.position + cameraOriginalLocalPosition + _rigidbody.velocity * deltaFixedTime;
        Camera.transform.position = Vector3Extensions.MoveTowards(Camera.transform.position, cameraTargetPosition, 0.5f, 10f, Time.deltaTime);
        ApplyRotation(inputLook);

        Vector3 desiredForward = Vector3.ProjectOnPlane(Camera.transform.forward, Vector3.up);

        transform.LookAt(transform.position + desiredForward, Vector3.up);
    }

    private void ApplyRotation(Vector2 inputLook)
    {
        Camera.transform.Rotate(inputLook.y, inputLook.x, 0, Space.Self);
        Camera.transform.LookAt(Camera.transform.position + Camera.transform.forward, Vector3.up);
    }

    public IEnumerator RotateKick(Vector2 kickOffset, float time)
    {
        float timestamp = Time.time;
        while (Time.time < timestamp + time)
        {
            ApplyRotation(Vector2.Lerp(Vector2.zero, kickOffset * Time.deltaTime, Mathf.Pow(1 - (Time.time - timestamp) / time, 2)));
            yield return new WaitForEndOfFrame();
        }
    }

    private void FixedUpdate()
    {
        lastFixedUpdate = Time.time;
        Vector3 targetVelocity = new Vector3(movementVelocity.x, _rigidbody.velocity.y, movementVelocity.z);
        //_rigidbody.AddForce(movementVelocity, ForceMode.Acceleration);
        _rigidbody.velocity = targetVelocity;
    }
}
