using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Vector3Extensions
{
    public static Vector3 MoveTowards(Vector3 origin, Vector3 target, float maxDistance, float moveSpeed, float deltaTime)
    {
        Vector3 desiredMovement = target - origin;
        float movementDistance = moveSpeed * deltaTime;
        if(desiredMovement.magnitude - movementDistance > maxDistance)
        {
            return target - Vector3.ClampMagnitude(desiredMovement, maxDistance);
        }
        return origin + Vector3.ClampMagnitude(desiredMovement.normalized * movementDistance, desiredMovement.magnitude);
    }
}
