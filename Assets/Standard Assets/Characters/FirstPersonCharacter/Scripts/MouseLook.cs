using System;
using System.Collections;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets.Characters.FirstPerson
{
    [Serializable]
    public class MouseLook
    {
        public float referenceFov = 90f;
        public float currentFov = 90f;
        public float XSensitivity = 2f;
        public float YSensitivity = 2f;
        public bool clampVerticalRotation = true;
        public float MinimumX = -90F;
        public float MaximumX = 90F;
        public bool smooth;
        public float smoothTime = 5f;
        public bool lockCursor = true;
        public Transform peekTransform;

        public Vector2 targetOffset;
        public Vector2 actualOffset;


        private Quaternion m_CharacterTargetRot;
        private Quaternion m_CameraTargetRot;
        private bool m_cursorIsLocked = true;
        private Transform character;
        private Transform camera;

        public void Init(Transform character, Transform camera)
        {
            this.character = character;
            this.camera = camera;
            m_CharacterTargetRot = character.localRotation;
            m_CameraTargetRot = camera.localRotation;
        }


        public void LookRotation()
        {
            float xRot = CrossPlatformInputManager.GetAxis("Mouse X") * XSensitivity;
            float yRot = CrossPlatformInputManager.GetAxis("Mouse Y") * YSensitivity;

            ApplyRotation(new Vector2(xRot, yRot) * (currentFov / referenceFov));

            UpdateCursorLock();
        }

        private void ApplyRotation(Vector2 rotation)
        {
            m_CameraTargetRot = Quaternion.LookRotation(camera.forward * 10 + camera.right * rotation.x + camera.up * rotation.y, peekTransform.up);

            m_CharacterTargetRot = Quaternion.Euler(0f, m_CameraTargetRot.eulerAngles.y, 0f);
            character.rotation = m_CharacterTargetRot;

            camera.rotation = m_CameraTargetRot;
            if (clampVerticalRotation)
                camera.localRotation = ClampRotationAroundXAxis(camera.localRotation);

        }

        public IEnumerator RotateKick(Vector2 kickOffset, float time)
        {
            float timestamp = Time.time;
            while(Time.time < timestamp + time)
            {
                ApplyRotation(Vector2.Lerp(Vector2.zero, kickOffset * Time.deltaTime, Mathf.Pow(1 -(Time.time - timestamp) / time, 2)));
                yield return new WaitForEndOfFrame();
            }
        }

        public void SetCursorLock(bool value)
        {
            lockCursor = value;
            if(!lockCursor)
            {//we force unlock the cursor if the user disable the cursor locking helper
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
        }

        public void UpdateCursorLock()
        {
            //if the user set "lockCursor" we check & properly lock the cursos
            if (lockCursor)
                InternalLockUpdate();
        }

        private void InternalLockUpdate()
        {
            if(Input.GetKeyUp(KeyCode.Escape))
            {
                m_cursorIsLocked = false;
            }
            else if(Input.GetMouseButtonUp(0))
            {
                m_cursorIsLocked = true;
            }

            if (m_cursorIsLocked)
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }
            else if (!m_cursorIsLocked)
            {
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
        }

        Quaternion ClampRotationAroundXAxis(Quaternion q)
        {
            q.x /= q.w;
            q.y /= q.w;
            q.z /= q.w;
            q.w = 1.0f;

            float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan (q.x);

            angleX = Mathf.Clamp (angleX, MinimumX, MaximumX);

            q.x = Mathf.Tan (0.5f * Mathf.Deg2Rad * angleX);

            return q;
        }

    }
}
