Shader "Hidden/HoloShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _NoiseTex("Noise Texture", 2D) = "white" {}
        _CircleCenter("Circle Center", Vector) = (0.5,0.5,0,0)
        _CircleRadius("Circle Radius", Float) = 0.1
        _NoiseScale("Noise Scale", Float) = 0.1
        _NoiseStrength("Noise Strength", Float) = 0.1
        _GlowScale("Glow Scale", Float) = 0.1
        _GlowStrength("Glow Strength", Float) = 0.1
            _GlowPower("Glow Power", Float) = 0.1
    }
    SubShader
    {
        // No culling or depth
        //Cull front
        ZWrite Off 
        Blend SrcAlpha OneMinusSrcAlpha
        //ZTest Always

        Tags { "RenderType"="Transparent" "Queue"="Transparent" "IgnoreProjector"="true"}

        Pass
        {
            CGPROGRAM
            #pragma vertex vert alpha
            #pragma fragment frag alpha

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            sampler2D _MainTex;
            sampler2D _NoiseTex;
            float2 _CircleCenter;
            float _CircleRadius;
            float _NoiseScale;
            float _NoiseStrength;
            float _GlowScale;
            float _GlowStrength;
            float _GlowPower;

            fixed4 frag(v2f i) : SV_Target
            {
                fixed4 noise = tex2D(_NoiseTex, i.uv);

                float2 retUv = i.uv;

                retUv -= _CircleCenter;
                retUv /= _CircleRadius;
                retUv += 0.5;

                //i.uv += 0.5;
                //i.uv /= _CircleRadius;
                //i.uv -= (_CircleCenter + 0.5) / _CircleRadius;
                //i.uv += 0.5;

                fixed4 col = tex2D(_MainTex, retUv);
                //float dst = distance(i.uv, _CircleCenter) * _CircleRadius;

                col.r = 1;
                col.g = 0;
                col.b = 0;
                //col.a *= 1 - dst * dst * 10;
                // just invert the colors
                //col.rgb = 1 - col.rgb;
                //col.a = viewDir;
                //col.a += 0.1;
                
                //col.a *= pow(tex2D(_NoiseTex, i.uv), 2);
                //col.g = pow(tex2D(_NoiseTex, i.uv * _GlowScale) * _GlowStrength, _GlowPower) * col.a;
                //col.b = col.g;

                if (col.a > tex2D(_NoiseTex, i.uv * _NoiseScale).r * _GlowStrength) {
                    col.a = pow(col.a, _GlowPower);
                }
                else {
                    col.a -= tex2D(_NoiseTex, i.uv * _NoiseScale) * _NoiseStrength;
                }

                return col;
            }
            ENDCG
        }
    }
}
