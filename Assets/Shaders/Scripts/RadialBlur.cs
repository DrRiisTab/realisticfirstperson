﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadialBlur : MonoBehaviour
{
    public Material effectMaterial;

    [Range(4, 16)]
    public int samples = 4;

    [Range(-1,1)]
    public float amount = 0;

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        effectMaterial.SetFloat("_EffectAmount", amount);
        Graphics.Blit(source, destination, effectMaterial);
    }
}
