Shader "Hidden/LensEffectShader"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_CameraDepthTexture("Camera Depth Texture", 2D) = "white" {}
		_Reticle("Reticle", 2D) = "white" {}

		_ShadeRadius("Shade Radius", Range(0.0, .5)) = 0.5
		_BlurRadius("Blur Radius", Range(0.0, 0.5)) = 0.5
		_ShadeCenter("Shade Center", Vector) = (.5, .5, 0, 0)

		_ImageOffset("Image Offset", Vector) = (0, 0, 0, 0)
		_ReticleOffset("Reticle Offset", Vector) = (0, 0, 0, 0)
		_ReticleSize("Reticle Size", Float) = 0.5
	}
		SubShader
		{
			Pass
			{
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag

				#include "UnityCG.cginc"

				struct appdata
				{
					float4 vertex : POSITION;
					float2 uv : TEXCOORD0;
				};

				struct v2f
				{
					float2 uv : TEXCOORD0;
					float4 vertex : SV_POSITION;
				};

				v2f vert(appdata v)
				{
					v2f o;
					o.vertex = UnityObjectToClipPos(v.vertex);
					o.uv = v.uv;
					return o;
				}

				sampler2D _MainTex;
				sampler2D _CameraDepthTexture;
				sampler2D _Reticle;
				float _ShadeRadius;
				float4 _ShadeCenter;
				float _BlurRadius;
				float4 _ImageOffset;
				float4 _ReticleOffset;
				float _ReticleSize;

				fixed4 frag(v2f i) : SV_Target
				{
					float2 center = float2(0.5, 0.5);

					fixed4 col = tex2D(_MainTex, i.uv - _ReticleOffset.xy);


				float distanceToCenter = distance(i.uv.xy, _ShadeCenter.xy + _ReticleOffset.xy) - _ShadeRadius;

				if (distanceToCenter > _BlurRadius) {
					col *= 0;
				}
				else if (distanceToCenter > 0) {
					col *= 1 - (distanceToCenter / _BlurRadius);
				}

				fixed4 reticleCol = tex2D(_Reticle, (i.uv - center - _ReticleOffset.xy + _ImageOffset.xy) * _ReticleSize + center);
				col.rgb = lerp(col.rgb, reticleCol.rgb, reticleCol.a);

				return col;
			}
			ENDCG
		}
		}
}
