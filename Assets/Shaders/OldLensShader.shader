Shader "Unlit/OldLensShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }

        AlphaToMask On
        ZWrite On

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog
            //#pragma require msaatex

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            struct fout {
                fixed4 color : COLOR;
                float depth : DEPTH;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            //Texture2DMS<float4> _MainTex;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fout frag(v2f i)
            {
                fout fo;
                // sample the texture
                fo.color = tex2D(_MainTex, i.uv);
                fo.depth = 1;
                //// apply fog
                //UNITY_APPLY_FOG(i.fogCoord, col);
                //
                float deltaX = 0.5 - i.uv.x;
                float deltaY = 0.5 - i.uv.y;
                if (deltaX * deltaX + deltaY * deltaY > 0.05) {
                    fo.color.rgb *= 0.1;
                  //col.rbg = 1 - col.rbg;
                }
                else {
                    fo.depth = 0;
                    fo.color.a = 0;
                }

                //fixed4 col = float4(1,1,1,1);
                //fixed4 col = _MainTex.Load(0,0);

                return fo;
            }
            ENDCG
        }
    }
}
