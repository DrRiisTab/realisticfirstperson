# Side project "RealisticFirstPerson"
The project contains some prototypes and experiments trying to create a realistic first person game.

## Disclaimer
The project is not meant to be used for a final product it's really just more a playground to experiment and gain experience. As there is no publication intended the project may contain some copyright protected resources and should therefore not be widely distributed.
The project covers some interesting ideas, but has no real project structure and the code is not optimized and there is no guarantee for any content.

# Demo Build
The archive Build.zip contains the compiled demo to give a possibility to test the features without having to build the project.

# Key Features

## Ballistics
- Realistic ballistic behaviour of projectiles fired from small arms.
	- Projectile mass
	- Projectile diameter (caliber)
	- Air friction coefficency including AnimationCurve for realistic transsonic behaviour values
	- Gravity constant
	- Air pressure
	- Calculated kinetic energy
	
- Projectile leaves Muzzle and not Camera :)
	
Planned improvements:

- Air density
	- Air pressure
	- Air temperature
	- Air humidity
- trajectory deflection by wind

## Holo-Sight
- Shader for drawing a "projected" reticle on transparent surface.
- As on real holo sights the reticle is always "drawn" on target even when not looking perfectly centered.
- Correct reticle scaling by official specifications (EOTECH: 68 MOA ring), even with changing Camera FOVs.

Planned improvements:

- Build zeroing capability (variable already prepared).
- More realistic reticle visualization.

## Riflescope
- Shader to apply effects on a RenderTexture used to render the lense.
	- Variable zoom
	- Multiple reticle textures to support multiple levels of detail depending on zoom level. Intended fov can be set individually.
	- Correct scaling of the reticle in all zoom levels and main Camera FOV. 
	- The reticle is drawn as FFP reticle.
	- Variable image plane which gives a better impression on looking through a scope than looking at a screen.
	- Parallax setting: When not looking perfectly centered through the scope the reticle is drawn slightly off, on objects not on the parallax plane.
	- "black ring"-behaviour when not looking through the scope with the correct eye relief.
	- Accurated Zeroing capability by adjusting the scope camera with "clicks".
	
Planned improvements:

- Make the "black ring"-behaviour more accurate by applying optics theory.
- Use parallax setting to blur objects outside of focus range.
- Find a way to greatly improve performance by changing the behaviour of the render pipeline to prevent unecessairy draw calls or use other visual tricks.

# Controls
| Control | Keybind |
|---|---|
| Movement| WASD |
| Sprint (hold)| Left shift|
| Switch Gun (Sniper Rifle/Assault Rifle)| F|
| ADS (hold)| RMB|
| Toggle fire mode| B|
| View zooming| Mouse wheel|
| Riflescope zooming| Home/End|
| Riflescope zeroing 0.1 MRAD| Page up/Page down|
| Riflescope zeroing 1.0 MRAD| Left shift + Page up/Page down|